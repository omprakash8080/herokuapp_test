package com.heady.ecom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.heady.ecom.R;
import com.heady.ecom.network.model.Variant;

import java.util.List;

/**
 * Created by om on 30/01/18.
 */

public class VarientAdapter extends RecyclerView.Adapter<VarientAdapter.RecyclerViewHolders> {

    private List<Variant> variants;
    private Context context;

    public VarientAdapter(Context context, List<Variant> products) {
        this.variants = products;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.varient_view_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.varientName.setText(variants.get(position).getColor());
        holder.price.setText(variants.get(position).getPrice()+"");
        holder.size.setText("Size : "+variants.get(position).getSize());

    }

    @Override
    public int getItemCount() {
        return this.variants.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView varientName;
        public TextView price;
        public TextView size;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            varientName = (TextView)itemView.findViewById(R.id.varient_name);
            price = (TextView)itemView.findViewById(R.id.text_price);
            size = (TextView)itemView.findViewById(R.id.varient_size);

        }

        @Override
        public void onClick(View view) {
//            Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
}