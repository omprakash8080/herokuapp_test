package com.heady.ecom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.heady.ecom.MainActivity;
import com.heady.ecom.MainActivity_;
import com.heady.ecom.MainApplication;
import com.heady.ecom.R;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.network.model.Product_;
import com.heady.ecom.network.model.event.ProductEvent;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.TinyDB;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

/**
 * Created by om on 30/01/18.
 */

public class RankingProductAdapter extends RecyclerView.Adapter<RankingProductAdapter.RecyclerViewHolders> {

    private List<Product_> products;
    private Context context;
    TinyDB tinyDB;
    HashMap productDataMap;

    public RankingProductAdapter(Context context, List<Product_> products) {
        this.products = products;
        this.context = context;
        this.productDataMap = MainActivity.productDataMap;
//        tinyDB = MainApplication.mainComponent.getTinyDB();
//        productDataMap = (HashMap) tinyDB.getObject(Constants.PRODUCT_MAP, HashMap.class);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {


        Product product = (Product) productDataMap.get(products.get(position).getId()+"");
        holder.countryName.setText(product.getName()+"");
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView countryName;
        public ImageView countryPhoto;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            countryName = (TextView)itemView.findViewById(R.id.country_name);
            countryPhoto = (ImageView)itemView.findViewById(R.id.country_photo);
        }

        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new ProductEvent(getPosition()));
//            Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
}