package com.heady.ecom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.heady.ecom.R;
import com.heady.ecom.network.model.Category;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.network.model.event.CategoryEvent;
import com.heady.ecom.network.model.event.ProductEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by om on 30/01/18.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.RecyclerViewHolders> {

    private List<Product> products;
    private Context context;

    public ProductAdapter(Context context, List<Product> products) {
        this.products = products;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.countryName.setText(products.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView countryName;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            countryName = (TextView)itemView.findViewById(R.id.country_name);
        }

        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new ProductEvent(getPosition()));
        }
    }
}