package com.heady.ecom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.heady.ecom.R;
import com.heady.ecom.network.model.Ranking;
import com.heady.ecom.network.model.event.ProductEvent;
import com.heady.ecom.network.model.event.RankingEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by om on 30/01/18.
 */

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.RecyclerViewHolders> {

    private List<Ranking> rankings;
    private Context context;

    public RankingAdapter(Context context, List<Ranking> rankings) {
        this.rankings = rankings;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ranking_view_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.rankingName.setText(rankings.get(position).getRanking());
    }

    @Override
    public int getItemCount() {
        return this.rankings.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView rankingName;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            rankingName = (TextView)itemView.findViewById(R.id.ranking_name);


        }

        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new RankingEvent(getPosition()));
//            Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
}