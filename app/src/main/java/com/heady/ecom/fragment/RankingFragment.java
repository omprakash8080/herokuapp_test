package com.heady.ecom.fragment;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.heady.ecom.MainApplication;
import com.heady.ecom.R;
import com.heady.ecom.adapter.RankingAdapter;
import com.heady.ecom.adapter.VarientAdapter;
import com.heady.ecom.network.facade.AppFacade;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.network.model.Ranking;
import com.heady.ecom.network.model.Variant;
import com.heady.ecom.network.model.event.ProductEvent;
import com.heady.ecom.network.model.event.RankingEvent;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.TinyDB;
import com.heady.ecom.util.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by om on 30/01/18.
 */


@EFragment(R.layout.fragment_ranking)
public class RankingFragment extends BaseFragment {

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    List<Ranking> rankings;
    RankingAdapter adapter;
    public static Context context;
    LinearLayoutManager linearLayoutManager;

    @AfterViews
    void init() {

        getActivity().setTitle("Ranking");
        recyclerViewInit();

    }


    private void recyclerViewInit() {
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        rankings = new ArrayList<>();
        adapter = new RankingAdapter(context, rankings);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }
    @Override
    public void onResume() {
        super.onResume();

        CategoryResponse categoryResponse = (CategoryResponse)tinyDB.getObject(CATEGORY_RES, CategoryResponse.class);
        if(categoryResponse!= null ) {
            List<Ranking> _rankings = categoryResponse.getRankings();
            if (_rankings.size() > 0) {
                rankings.addAll(_rankings);
                adapter.notifyDataSetChanged();
            }
        }

    }

        @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(RankingEvent rankingEvent) {
        tinyDB.putInt(Constants.SELECTED_RANKING_INDEX, rankingEvent.getPosition());
        switchFragment(new RankingProductFragment_(), true, R.id.frame_container, false);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(Throwable error) {

    }

}
