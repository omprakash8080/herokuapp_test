package com.heady.ecom.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.heady.ecom.MainApplication;
import com.heady.ecom.R;
import com.heady.ecom.util.AsyncLoader;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.TinyDB;

public abstract class BaseFragment extends Fragment implements Constants {

    public TinyDB tinyDB;
    protected Context context;
    private AsyncLoader asyncLoader;

    @Override
    public void onAttach(Context context) {
        MainApplication.mainComponent.inject((Activity)context);
        this.context = context;
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        asyncLoader = MainApplication.mainComponent.getAsyncLoader();
        tinyDB = MainApplication.mainComponent.getTinyDB();

    }

    public void showLoader() {
        try {
            asyncLoader.show();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (asyncLoader != null && asyncLoader.isShowing()) {
                asyncLoader.dismiss();
            }
        }
    }

    public void hideLoader() {
        try {
            if (asyncLoader != null && asyncLoader.isShowing()) {
                asyncLoader.dismiss();
                asyncLoader.hide();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate) {

        if (getActivity() != null) {
            if (!getActivity().isFinishing()) {
                FragmentTransaction fragmentTransaction = getActivity()
                        .getSupportFragmentManager().beginTransaction();
                if (animate) {
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                fragmentTransaction.replace(layout_id, fragment,
                        fragment.getTag());
                if (addtoBackStack) {
                    fragmentTransaction.addToBackStack(fragment.getTag());
                }

                fragmentTransaction.commit();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideLoader();
    }
}
