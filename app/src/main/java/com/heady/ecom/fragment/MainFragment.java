package com.heady.ecom.fragment;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.heady.ecom.MainApplication;
import com.heady.ecom.R;
import com.heady.ecom.network.facade.AppFacade;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.util.TinyDB;
import com.heady.ecom.util.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by om on 30/01/18.
 */


@EFragment(R.layout.fragment_main)
public class MainFragment extends BaseFragment {

    public static Context context;

    @ViewById(R.id.txt_category)
    RelativeLayout txtCategory;

    @ViewById(R.id.txt_ranking)
    RelativeLayout txtRanking;


    @AfterViews
    void init() {
        getActivity().setTitle("Home");

    }


    @Click({R.id.txt_category})
    void categoryClicked() {
        switchFragment(new CategoryFragment_(), true, R.id.frame_container, false);
    }


    @Click({R.id.txt_ranking})
    void rankingClicked() {
        switchFragment(new RankingFragment_(), true, R.id.frame_container, false);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onResume() {
        super.onResume();

        CategoryResponse response = (CategoryResponse)tinyDB.getObject(CATEGORY_RES, CategoryResponse.class);

        if(response != null ){

            if(response.getCategories() != null && response.getCategories().size()>0){

                txtCategory.setVisibility(View.VISIBLE);
            }else {
                txtCategory.setVisibility(View.INVISIBLE);
            }

            if(response.getRankings() != null && response.getRankings().size()>0){

                txtRanking.setVisibility(View.VISIBLE);
            }else {
                txtRanking.setVisibility(View.INVISIBLE);
            }


            TinyDB tinyDB = MainApplication.mainComponent.getTinyDB();
            tinyDB.putObject(CATEGORY_RES, response);

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(CategoryResponse response) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(Throwable error) {


    }

}
