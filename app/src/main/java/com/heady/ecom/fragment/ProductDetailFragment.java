package com.heady.ecom.fragment;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.heady.ecom.R;
import com.heady.ecom.adapter.VarientAdapter;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.network.model.Variant;
import com.heady.ecom.util.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by om on 30/01/18.
 */


@EFragment(R.layout.fragment_detail_product)
public class ProductDetailFragment extends BaseFragment {

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @ViewById(R.id.product_name)
    TextView productName;

    @ViewById(R.id.vat)
    TextView vat;

    @ViewById(R.id.date)
    TextView date;

    List<Variant> variants;
    VarientAdapter adapter;
    public static Context context;
    LinearLayoutManager linearLayoutManager;

    @AfterViews
    void init() {

        context = getActivity();
        getActivity().setTitle("Detail");
        recyclerViewInit();

    }

    private void recyclerViewInit() {
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        variants = new ArrayList<>();
        adapter = new VarientAdapter(context, variants);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();

        CategoryResponse categoryResponse = (CategoryResponse)tinyDB.getObject(CATEGORY_RES, CategoryResponse.class);
        int selected_category_index = tinyDB.getInt(Constants.SELECTED_CATEGORY_INDEX);
        int selected_product_index = tinyDB.getInt(Constants.SELECTED_PRODUCT_INDEX);

        Product product = categoryResponse.getCategories().get(selected_category_index).getProducts().get(selected_product_index);
        if(categoryResponse.getCategories() != null && product.getVariants().size()> 0){
            variants.addAll(product.getVariants());
            adapter.notifyDataSetChanged();
        }


        productName.setText(product.getName());
        date.setText(product.getDateAdded());
        vat.setText(product.getTax().getName()+ " "+product.getTax().getValue());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(CategoryResponse response) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(Throwable error) {


    }

}
