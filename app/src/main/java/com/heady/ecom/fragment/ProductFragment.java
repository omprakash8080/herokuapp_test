package com.heady.ecom.fragment;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.heady.ecom.MainApplication;
import com.heady.ecom.R;
import com.heady.ecom.adapter.CategoryAdapter;
import com.heady.ecom.adapter.ProductAdapter;
import com.heady.ecom.network.facade.AppFacade;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.Category;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.network.model.event.CategoryEvent;
import com.heady.ecom.network.model.event.ProductEvent;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.TinyDB;
import com.heady.ecom.util.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by om on 30/01/18.
 */


@EFragment(R.layout.fragment_product)
public class ProductFragment extends BaseFragment {


    IAppFacade appFacade;
    public static Context context;


    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;


    GridLayoutManager gridLayoutManager;

    List<Product> products;
    ProductAdapter adapter;

    @AfterViews
    void init() {
        getActivity().setTitle("Product");
        recyclerViewInit();

    }

    private void recyclerViewInit() {
        gridLayoutManager = new GridLayoutManager(context, 3);

        recyclerView.setLayoutManager(gridLayoutManager);

        products = new ArrayList<>();
        adapter = new ProductAdapter(context, products);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();

        CategoryResponse categoryResponse = (CategoryResponse)tinyDB.getObject(CATEGORY_RES, CategoryResponse.class);
        int selected_index = tinyDB.getInt(Constants.SELECTED_CATEGORY_INDEX);
        if(categoryResponse.getCategories() != null && categoryResponse.getCategories().get(selected_index).getProducts().size()> 0){
            products.addAll(categoryResponse.getCategories().get(selected_index).getProducts());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(ProductEvent productEvent) {
        tinyDB.putInt(Constants.SELECTED_PRODUCT_INDEX, productEvent.getPosition());
        switchFragment(new ProductDetailFragment_(), true, R.id.frame_container, false);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(Throwable error) {


    }

}
