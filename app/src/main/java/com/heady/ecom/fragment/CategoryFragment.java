package com.heady.ecom.fragment;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.heady.ecom.R;
import com.heady.ecom.adapter.CategoryAdapter;
import com.heady.ecom.network.facade.AppFacade;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.Category;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.network.model.event.CategoryEvent;
import com.heady.ecom.util.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by om on 30/01/18.
 */


@EFragment(R.layout.fragment_category)
public class CategoryFragment extends BaseFragment {


    IAppFacade appFacade;
    public static Context context;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    GridLayoutManager gridLayoutManager;

    List<Category> categories;
    CategoryAdapter adapter;

    @AfterViews
    void init() {
        context = getActivity();
        getActivity().setTitle("Category");
        recyclerViewInit();
        appFacade = new AppFacade();
        appFacade.getCategory();

    }

    private void recyclerViewInit() {
        gridLayoutManager = new GridLayoutManager(context, 3);

        recyclerView.setLayoutManager(gridLayoutManager);

        categories = new ArrayList<>();
        adapter = new CategoryAdapter(context, categories);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }


    @Override
    public void onResume() {
        super.onResume();

        CategoryResponse categoryResponse = (CategoryResponse)tinyDB.getObject(CATEGORY_RES, CategoryResponse.class);
        if(categoryResponse != null && categoryResponse.getCategories().size()> 0){
            categories.addAll(categoryResponse.getCategories());
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(CategoryEvent categoryEvent) {
        tinyDB.putInt(Constants.SELECTED_CATEGORY_INDEX, categoryEvent.getPosition());
        switchFragment(new ProductFragment_(), true, R.id.frame_container, false);

    }


}
