package com.heady.ecom;

import android.app.Application;

import com.heady.ecom.components.DaggerMainComponent;
import com.heady.ecom.components.MainComponent;
import com.heady.ecom.components.module.AppModule;
import com.heady.ecom.components.module.NetModule;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.Util;


/**
 * Created by om on 30/01/18.
 */

public class MainApplication extends Application implements Constants {


    public static MainComponent mainComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        mainComponent = DaggerMainComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL))
                .build();

        Util.printString("tinyDB", mainComponent.getTinyDB()+"");
        Util.printString("app", mainComponent.getApplication()+"");

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }
}
