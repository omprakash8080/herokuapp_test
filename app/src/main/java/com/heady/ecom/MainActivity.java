package com.heady.ecom;

import com.heady.ecom.fragment.MainFragment_;
import com.heady.ecom.network.facade.AppFacade;
import com.heady.ecom.network.facade.IAppFacade;
import com.heady.ecom.network.model.Category;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.network.model.Product;
import com.heady.ecom.util.TinyDB;
import com.heady.ecom.util.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;


@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity{

    IAppFacade appFacade;
    public static HashMap productDataMap = new HashMap<String, Product>();

    @AfterViews
    void init() {

        appFacade = new AppFacade();
        appFacade.getCategory();
        switchFragment(new MainFragment_(), false, R.id.frame_container, false);
//        switchFragment(new ProductDetailFragment_(), true, R.id.frame_container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseEvent(CategoryResponse response) {

            Util.printString("res", response.getCategories().get(0).getName()+"");

            TinyDB tinyDB = MainApplication.mainComponent.getTinyDB();
            tinyDB.putObject(CATEGORY_RES, response);

             cacheAllproduct(response);

    }


    @Background
    void cacheAllproduct(CategoryResponse response) {

        for(Category category : response.getCategories()){

            for(Product product : category.getProducts()){
                productDataMap.put(product.getId()+"", product);
            }
        }

//        TinyDB tinyDB = MainApplication.mainComponent.getTinyDB();
//        tinyDB.putObject(PRODUCT_MAP, productDataMap);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(Throwable error) {

    }

}
