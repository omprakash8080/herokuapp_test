package com.heady.ecom;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.heady.ecom.util.AsyncLoader;
import com.heady.ecom.util.Constants;
import com.heady.ecom.util.TinyDB;


public class BaseActivity extends AppCompatActivity implements Constants {

    public TinyDB tinyDB;
    private AsyncLoader asyncLoader;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        MainApplication.mainComponent.inject(this);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        asyncLoader = MainApplication.mainComponent.getAsyncLoader();
        tinyDB = MainApplication.mainComponent.getTinyDB();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
    }


    public void showLoader() {
        try {
            if(asyncLoader != null) {
                asyncLoader.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (asyncLoader != null && asyncLoader.isShowing()) {
                asyncLoader.dismiss();
            }
        }
    }

    public void hideLoader() {
        try {
            if (asyncLoader != null && asyncLoader.isShowing()) {
                asyncLoader.dismiss();
                asyncLoader.hide();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate) {

        setTitle(getString(R.string.app_name));
        if (!isFinishing()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                                                        .beginTransaction();
            if (animate) {
                //fragmentTransaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
            }
            fragmentTransaction.replace(layout_id, fragment);
            if (addtoBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }
    }


}