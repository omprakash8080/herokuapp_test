package com.heady.ecom.components.module;

import android.app.Application;

import com.heady.ecom.util.AsyncLoader;
import com.heady.ecom.util.TinyDB;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by om on 30/01/18.
 */

@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    TinyDB providesTinyDB() {
        TinyDB tinyDB = new TinyDB(mApplication);
        return tinyDB;
    }

    @Provides
    @Singleton
    AsyncLoader providesAsyncLoader() {
        AsyncLoader asyncLoader = AsyncLoader.dialog(mApplication);
        return asyncLoader;
    }
}
