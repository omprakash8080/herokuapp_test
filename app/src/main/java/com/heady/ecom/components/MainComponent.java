package com.heady.ecom.components;


import android.app.Activity;
import android.app.Application;


import com.heady.ecom.components.module.AppModule;
import com.heady.ecom.components.module.NetModule;
import com.heady.ecom.network.AppApi;
import com.heady.ecom.util.AsyncLoader;
import com.heady.ecom.util.TinyDB;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface MainComponent {

    //AppModule
    Application getApplication();
    TinyDB getTinyDB();
    AsyncLoader getAsyncLoader();

   //NetModule
    AppApi getApiService();
    Retrofit retrofit();
    OkHttpClient okHttpClient();
    void inject(Activity activity);
}