package com.heady.ecom.util;

/**
 * Created by om on 30/01/18.
 */

public interface Constants {
    String BASE_URL = "https://stark-spire-93433.herokuapp.com";
    String CATEGORY_RES = "CATEGORY_RES";
    String SELECTED_CATEGORY_INDEX = "SELECTED_CATEGORY_INEDX";
    String SELECTED_PRODUCT_INDEX = "SELECTED_PRODUCT_INDEX";
    String SELECTED_RANKING_INDEX = "SELECTED_RANKING_INDEX";
    String PRODUCT_MAP = "PRODUCT_MAP";
}
