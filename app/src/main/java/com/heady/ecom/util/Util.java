package com.heady.ecom.util;

import android.util.Log;

/**
 * Created by om on 30/01/18.
 */

public class Util implements Constants {

    public static void printString(String tag, String str) {
        logLargeString(tag, str);
    }

    public static void logLargeString(String tag, String str) {
        if (str.length() > 3000) {
            Log.e(tag, str.substring(0, 3000));
            logLargeString(tag, str.substring(3000));
        } else {
            Log.e(tag, str); // continuation
        }
    }

}
