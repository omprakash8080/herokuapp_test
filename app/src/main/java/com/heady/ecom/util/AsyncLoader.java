package com.heady.ecom.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.heady.ecom.R;

/**
 * Created by om on 30/01/18.
 */

public class AsyncLoader extends Dialog {
    public AsyncLoader(Context context) {
        super(context);
    }

    public AsyncLoader(Context context, int theme) {
        super(context, theme);
    }

    public void setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            findViewById(R.id.message).setVisibility(View.VISIBLE);
            TextView txt = findViewById(R.id.message);
            txt.setText(message);
            txt.invalidate();
        }
    }

    public static AsyncLoader show(Context context) {
        return show(context, "", true, null);
    }

    public static AsyncLoader show(Context context, CharSequence message,
                                   boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        AsyncLoader dialog = dialog(context, message, cancelable, cancelListener);
        dialog.show();

        return dialog;
    }

    public static AsyncLoader dialog(final Context context) {
        return dialog(context, "", true, null);
    }

    public static AsyncLoader dialog(final Context context, final CharSequence message,
                                     final boolean cancelable,
                                     final DialogInterface.OnCancelListener cancelListener) {
        AsyncLoader dialog = new AsyncLoader(context, R.style.FragmentDialog);
        dialog.setTitle("");
        dialog.setContentView(R.layout.async_loader);
        if (message == null || message.length() == 0) {
            dialog.findViewById(R.id.message).setVisibility(View.GONE);
        } else {
            TextView txt = dialog.findViewById(R.id.message);
            txt.setText(message);
        }
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        return dialog;
    }

}