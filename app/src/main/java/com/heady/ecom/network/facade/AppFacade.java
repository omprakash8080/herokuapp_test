package com.heady.ecom.network.facade;

import com.heady.ecom.MainApplication;
import com.heady.ecom.network.AppApi;
import com.heady.ecom.network.model.CategoryResponse;
import com.heady.ecom.util.Util;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by om on 30/01/18.
 */

public class AppFacade implements IAppFacade {

    AppApi appApi;

    public AppFacade(){
        appApi = MainApplication.mainComponent.getApiService();
        Util.printString("api", appApi+"");

    }

    @Override
    public void getCategory() {

        Call<CategoryResponse> callback  = appApi.getCategoryResponse();
        callback.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                EventBus.getDefault().post(response.body());

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Util.printString("facade err", t+"");
                EventBus.getDefault().post(t);

            }
        });

    }



}
