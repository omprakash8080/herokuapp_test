package com.heady.ecom.network;

import com.heady.ecom.network.model.CategoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by om on 30/01/18.
 */

public interface AppApi {

    @GET("/json")
    Call<CategoryResponse> getCategoryResponse();

}
