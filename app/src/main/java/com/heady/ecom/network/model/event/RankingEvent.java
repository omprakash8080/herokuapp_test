package com.heady.ecom.network.model.event;

/**
 * Created by om on 30/01/18.
 */

public class RankingEvent {
   Integer position;


   public RankingEvent(int position){
       this.position = position;
   }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
