package com.heady.ecom.network.model.event;

/**
 * Created by om on 30/01/18.
 */

public class ProductEvent {
   Integer position;


   public ProductEvent(int position){
       this.position = position;
   }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
